#!/bin/bash

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   exit 1
fi

DDIR=/var/quorum

config() {
	json $1 < $DDIR/config.json
}

echo "[*] Starting Constellation Nodes"
sudo constellation-start

echo "[*] Starting Geth Nodes"
ARGS="--nodiscover --raft --rpc --rpcaddr 0.0.0.0 --rpcapi admin,db,eth,debug,miner,net,shh,txpool,personal,web3,quorum --emitcheckpoints"
PRIVATE_CONFIG=$DDIR/1/constellation.ipc nohup geth \
	--permissioned \
	--datadir $DDIR/1/node $ARGS \
	--raftport 50401 \
	--rpcport 21001 \
	--port 22001 \
	--unlock 0 \
	--password password.txt 2>>$DDIR/logs/1.log &
PRIVATE_CONFIG=$DDIR/2/constellation.ipc nohup geth \
	--permissioned \
	--datadir $DDIR/2/node $ARGS \
	--raftport 50402 \
	--rpcport 21002 \
	--port 22002 \
	--unlock 0 \
	--password password.txt 2>>$DDIR/logs/2.log &
echo "[*] Nodes started. Run geth attach $DDIR/1/node/geth.ipc to access the geth console"
