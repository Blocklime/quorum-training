#!/bin/bash

DDIR=/var/quorum

config() {
	json $1 < config.json
}

#Create new genesis.json at root dir (Redirect to configured download link in the future)
#Only run when genesis file is properly configured
geth --datadir $DDIR/1/node init $DDIR/raft-genesis.json
geth --datadir $DDIR/2/node init $DDIR/raft-genesis.json
mkdir -p $DDIR/logs

ARGS="--nodiscover --raft --rpc --rpcaddr 0.0.0.0 "
ARGS=$ARGS" --rpcapi admin,db,eth,debug,miner,net,shh,txpool,personal,web3,quorum --emitcheckpoints"
nohup geth --datadir $DDIR/1/node $ARGS \
	--raftport 50401 \
	--rpcport 21001 \
	--port 22001 \
	--unlock 0 \
	--password password.txt 2>>$DDIR/logs/1.log &
nohup geth --datadir $DDIR/2/node $ARGS \
	--raftport 50402 \
	--rpcport 21002 \
	--port 22002 \
	--unlock 0 \
	--password password.txt 2>>$DDIR/logs/2.log &

sleep 5
sudo killall geth

#Get enodeid from nodekey
nodekey=$(sudo cat $DDIR/1/node/geth/nodekey)
enodeid=$(bootnode -nodekeyhex $nodekey -writeaddress)
echo $enodeid > enodeid.txt
nodekey2=$(sudo cat $DDIR/2/node/geth/nodekey)
enodeid2=$(bootnode -nodekeyhex $nodekey2 -writeaddress)
echo $enodeid2 >> enodeid.txt
echo "Save the enode ID to be used in permissioned-nodes.json"
echo "[*] Node initialized, run quorum-start to start the node after configuration."
