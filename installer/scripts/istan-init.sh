#!/bin/bash
#Create new genesis.json at root dir (Redirect to configured download link in the future)
#Only run when genesis file is properly configured

DDIR=/var/quorum

config() {
	json $1 < config.json
}

mkdir -p $DDIR/{keystore,geth}
geth --datadir $DDIR/node init $DDIR/istan-genesis.json
#cp 0/nodekey $DDIR/node/geth/nodekey
#rm -rf 0/
mkdir -p $DDIR/logs

ARGS="--nodiscover --syncmode full --mine --rpc --rpcaddr 0.0.0.0 "
ARGS=$ARGS" --rpcapi admin,db,eth,debug,miner,net,shh,txpool,personal,web3,quorum,istanbul"
nohup geth --datadir $DDIR/node $ARGS \
	--rpcport `config RPC_port` \
	--port `config P2P_port` \
	--unlock 0 \
	--password password.txt 2>>$DDIR/logs/1.log &

sleep 5
sudo killall geth

#Get enodeid from nodekey
nodekey=$(sudo cat $DDIR/node/geth/nodekey)
enodeid=$(bootnode -nodekeyhex $nodekey -writeaddress)
echo $enodeid > enodeid.txt
echo "Save the enode ID to be used in permissioned-nodes.json"

echo "[*] Node initialized, run quorum-start to start the node after configuration."
