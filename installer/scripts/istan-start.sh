#!/bin/bash

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   exit 1
fi

DDIR=/var/quorum

config() {
	json $1 < $DDIR/config.json
}

echo "[*] Starting Constellation Nodes"
sudo constellation-start

echo "[*] Starting Geth Nodes"
ARGS="--nodiscover --syncmode full --mine --rpc --rpcaddr 0.0.0.0 "
ARGS=$ARGS" --rpcapi admin,db,eth,debug,miner,net,shh,txpool,personal,web3,quorum,istanbul"
PRIVATE_CONFIG=$DDIR/constellation.ipc nohup geth \
	--permissioned \
	--datadir $DDIR/node $ARGS \
	--rpcport `config RPC_port` \
	--port `config P2P_port` \
	--unlock 0 \
	--password password.txt 2>>$DDIR/logs/1.log &

echo "[*] Nodes started. Run geth attach $DDIR/node/geth.ipc to access the geth console"
