#!/bin/bash
#Create a permissioned-nodes.json at root dir

DDIR=/var/quorum

mkdir -p $DDIR/node/
cp istan-static-nodes.json $DDIR/node/static-nodes.json
cp istan-static-nodes.json $DDIR/node/permissioned-nodes.json

#Generate Constellation Keynode
#Reference: https://github.com/jpmorganchase/constellation#generating-keys
constellation-node --generatekeys=node
mv node.pub $DDIR
mv node.key $DDIR

#Generate new keystore/account
geth --datadir $DDIR/node/ account new --password password.txt > setup.log
cp setup.log $DDIR
