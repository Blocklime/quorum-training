#!/bin/bash

DDIR=/var/quorum

istanbul setup --num 1 --nodes --verbose  --save 
mv static-nodes.json istan-static-nodes.json
mv genesis.json $DDIR/istan-genesis.json
mkdir -p $DDIR/node/geth/ && cp 0/nodekey $DDIR/node/geth/nodekey
rm -rf 0/