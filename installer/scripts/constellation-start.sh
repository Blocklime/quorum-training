#!/bin/bash
set -u
set -e

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   exit 1
fi

DDIR=/var/quorum

CONSTELLATION_IP=$(json constellation.IP_address < $DDIR/config.json)
CONSTELLATION_PORT=$(json constellation.port < $DDIR/config.json)
OTHERNODES=$(json othernodes < $DDIR/config.json)
IP_REGEXP='^[0-9][0-9]?[0-9]?\.[0-9][0-9]?[0-9]?\.[0-9][0-9]?[0-9]?\.[0-9][0-9]?[0-9]?$'

if [ "$CONSTELLATION_IP" == "AUTO_PUBLIC" ]; then
	CONSTELLATION_IP=$(
		dig +short myip.opendns.com @resolver1.opendns.com 2>/dev/null || 
		curl -s http://whatismyip.akamai.com/
	)
elif [ "$CONSTELLATION_IP" == "AUTO_LAN" ]; then
	CONSTELLATION_IP=$(hostname -i)
elif echo $CONSTELLATION_IP | grep -E $IP_REGEXP; then
	# do nothing. The IP address is in correct format
	true
else
	echo "ERROR: Unsupported value for constellation.IP_address!"
	echo "Please check your $DDIR/config.json file and ensure that the value for"
	echo "\"constellation.IP_address\" is either an IP address or \"AUTO_LAN\" or \"AUTO_PUBLIC\"."
	echo "  Note: AUTO_LAN will automatically set the value to the IP address of your main interface"
    echo "  while AUTO_PUBLIC will try set the value to your public IP address." 
	exit 1
fi	

for i in {1..2}
do
    mkdir -p $DDIR
    mkdir -p $DDIR/logs
    rm -f "$DDIR/$i/constellation.ipc"

    CMD="constellation-node --url=https://127.0.0.$i:900$i/ --port=900$i "
	CMD=$CMD" --workdir=$DDIR/$i --socket=constellation.ipc --publickeys=node.pub --privatekeys=node.key "
	CMD=$CMD" --othernodes=https://127.0.0.1:9001/"
	
    echo "$CMD"
    $CMD >> $DDIR/logs/constellation$i.log 2>&1 &
	echo "Constellation started"
done

DOWN=true
while $DOWN;
do
    sleep 0.5
    DOWN=false
    for i in {1..2}
    do
	if [ ! -S "$DDIR/$i/constellation.ipc" ]; then
            DOWN=true
	fi
    done
done
