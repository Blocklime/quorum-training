#!/bin/bash

DDIR=/var/quorum

#Create a permission-nodes.json at root dir
for i in {1..2}
do
	mkdir -p $DDIR/$i/node/
	cp static-nodes.json $DDIR/$i/node/static-nodes.json
	cp static-nodes.json $DDIR/$i/node/permissioned-nodes.json
done
cp ./raft-genesis.json $DDIR/raft-genesis.json

#Generate Constellation Keynode
#Reference: https://github.com/jpmorganchase/constellation#generating-keys
constellation-node --generatekeys=node
mv node.pub $DDIR/1
mv node.key $DDIR/1

constellation-node --generatekeys=node
mv node.pub $DDIR/2
mv node.key $DDIR/2

#Generate new keystore/account
geth --datadir $DDIR/1/node/ account new --password password.txt > setup.log
geth --datadir $DDIR/2/node/ account new --password password.txt >> setup.log
cp setup.log $DDIR
