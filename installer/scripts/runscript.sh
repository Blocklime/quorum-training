#!/bin/bash

DDIR=/var/quorum

PRIVATE_CONFIG=$DDIR/constellation.ipc geth --exec "loadScript(\"$1\")" attach ipc:$DDIR/node/geth.ipc