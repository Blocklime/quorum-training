#!/bin/bash

DDIR=/var/quorum

rm $DDIR/tls-client-cert.pem
rm $DDIR/tls-client-key.pem
rm $DDIR/tls-known-clients
rm $DDIR/tls-known-servers
rm $DDIR/tls-server-cert.pem
rm $DDIR/tls-server-key.pem
