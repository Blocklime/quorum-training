#!/bin/bash

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root"
   exit 1
fi

OS=$(./dep/os-version.sh | cut -f 1 -d -)
DDIR=/var/quorum

if [ "$OS" == "ubuntu" ]; then
  echo "[*] Installing Dependencies on $OS"
  bash ./dep/dep-ubuntu.sh
elif [ "$OS" == "debian" ]; then
  echo "[*] Installing Dependencies on $OS"
  bash ./dep/dep-debian.sh
elif [ "$OS" == "fedora" ]; then
  echo "[*] Installing Dependencies on $OS"
  bash ./dep/dep-fedora.sh
else
  echo "ERROR: Your operating system ($OS) is not supported.."
  echo "exiting..."
  exit 1
fi

CONSENSUS=$(json consensus < config.json)

if [ "$CONSENSUS" == "raft" ]; then
  echo "[*] Setting Up Quorum"
  bash ./scripts/raft-setup.sh
elif [ "$CONSENSUS" == "istanbul" ]; then
  while :
  do
    echo -e "Supported Option: \n (1) Y (Yes) \n (2) N (No) \n"
    read -p "Are you setting the first node in the network?: " option

    if [ "$option" -eq 1 ]; then
      echo "[*] Setting Up First Node"
      bash ./istan-gen.sh
      break
    elif [ "$option" -eq 2 ]; then
      break
    else
      echo "Please input only numbers from 1 to 2."
    fi
  done

  echo "[*] Setting Up Quorum"
  bash ./scripts/istan-setup.sh
else
  echo "ERROR: Consensus not supported."
  echo "Please check your config.json file and ensure that the"
  echo "value of \"consensus\" is either \"raft\" or \"istanbul\""
  exit 1
fi

mkdir -p $DDIR
cp config.json $DDIR

echo "Setup complete. Initializing node..."

if [ "$CONSENSUS" == "raft" ]; then
	bash ./scripts/raft-init.sh
	cp ./scripts/raft-start.sh $DDIR
  cp ./password.txt $DDIR
	rm -rf /sbin/quorum-start
	ln -s $DDIR/raft-start.sh /sbin/quorum-start
elif [ "$CONSENSUS" == "istanbul" ]; then
	bash ./scripts/istan-init.sh
	cp ./scripts/istan-start.sh $DDIR
  cp ./password.txt $DDIR
	rm -rf /sbin/quorum-start
	ln -s $DDIR/istan-start.sh /sbin/quorum-start
fi

cp ./scripts/constellation-start.sh $DDIR
rm -rf /sbin/constellation-start
ln -s $DDIR/constellation-start.sh /sbin/constellation-start

cp ./scripts/quorum-stop.sh $DDIR
rm -rf /sbin/quorum-stop
ln -s $DDIR/quorum-stop.sh /sbin/quorum-stop
