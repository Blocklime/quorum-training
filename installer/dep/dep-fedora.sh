#!/bin/bash
#Install Dependencies
yum update
#build-essential equivalent
yum groupinstall -y "Development Tools"
yum install -y unzip libdb-devel
dnf install -y libsodium-devel leveldb-devel zlib-devel ncurses-devel psmisc openssh-server pdsh curl
mv /usr/lib64/libsodium.so.23 /usr/lib64/libsodium.so.18

#Install nodejs
curl --silent --location https://rpm.nodesource.com/setup_8.x | sudo bash
sudo yum -y install nodejs
sudo npm install -g json pm2

#Install solc
dnf install -y snapd
rm -rf /snap
ln -s /var/lib/snapd/snap /snap
snap install solc

#Replace with solc compatible file
cp /var/lib/snapd/snap/core/5145/lib/x86_64-linux-gnu/libtinfo.so.5 /usr/lib64/libtinfo.so.5
cp /var/lib/snapd/snap/core/5145/usr/lib/x86_64-linux-gnu/libdb-5.3.so /usr/lib64/libdb-5.3.so

#Install Wrk
yum install openssl-devel git -y
git clone https://github.com/wg/wrk.git wrk
cd wrk
sudo make
# move the executable to somewhere in your PATH, ex:
sudo cp wrk /usr/local/bin
cd ..
sudo rm -rf wrk/

# install constellation
CVER="0.3.2"
CREL="constellation-$CVER-ubuntu1604"
wget -q https://github.com/jpmorganchase/constellation/releases/download/v$CVER/$CREL.tar.xz
tar xfJ $CREL.tar.xz
cp $CREL/constellation-node /usr/local/bin && chmod 0755 /usr/local/bin/constellation-node
rm -rf $CREL
rm -f $CREL.tar.xz

# install golang
GOREL=go1.9.3.linux-amd64.tar.gz
wget -q https://dl.google.com/go/$GOREL
tar xfz $GOREL
mv go /usr/local/go
rm -f $GOREL
rm -rf go/
PATH=$PATH:/usr/local/go/bin
echo 'PATH=$PATH:/usr/local/go/bin' >> /home/$SUDO_USER/.bashrc

# install istanbul-Tools
pwd=$(pwd)
cd
go get github.com/getamis/istanbul-tools/cmd/istanbul
cd go/src/github.com/getamis/istanbul-tools
make
cp build/bin/istanbul /usr/local/bin
cd $pwd

# make/install quorum
git clone https://github.com/jpmorganchase/quorum.git
pushd quorum >/dev/null
git checkout tags/v2.0.1
make all
cp build/bin/geth /usr/local/bin
cp build/bin/bootnode /usr/local/bin
popd >/dev/null
rm -rf quorum/

# install Porosity
wget -q https://github.com/jpmorganchase/quorum/releases/download/v1.2.0/porosity
mv porosity /usr/local/bin && chmod 0755 /usr/local/bin/porosity

#Initializing Server
service ssh start

#Initializing Files for Authorized Keys
mkdir -p ~/.ssh && chmod 700 ~/.ssh && touch ~/.ssh/authorized_keys && chmod 600 ~/.ssh/authorized_keys
