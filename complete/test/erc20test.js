const ERC20 = artifacts.require("ERC20");

contract("ERC20", function(accounts) {
    it("testContractCreation", () => {
        return ERC20.deployed().then(async token => {
            const totalSupply = await token.totalSupply();
            const name = await token.name();
            const symbol = await token.symbol();
            const decimals = await token.decimals();
            assert.equal(totalSupply, 10000, "Total supply is not the 10000!");
            assert.equal(name, "ERC20 Token", "Token name is not ERC20 Token!");
            assert.equal(symbol, "ERC20", "Token symbol is not ERC20!");
            assert.equal(decimals, 8, "Token decimals is not 8!");
        });
    });

    it("testOwnerShouldHaveAllToken", () => {
        return ERC20.deployed().then(async token => {
            const balance = await token.balanceOf(accounts[0]);
            assert.equal(
                balance,
                10000,
                "Owner supply is not the same as total supply!"
            );
        });
    });

    it("testTokenTransfer", () => {
        return ERC20.deployed().then(async token => {
            const ownerBalance = await token.balanceOf(accounts[0]);
            const userBalance = await token.balanceOf(accounts[1]);
            await token.transfer(accounts[1], 10);
            const newOwnerBalance = await token.balanceOf(accounts[0]);
            const newUserbalance = await token.balanceOf(accounts[1]);
            assert.equal(
                ownerBalance.toNumber() - 10,
                newOwnerBalance.toNumber(),
                "Owner balance is not correct"
            );
            assert.equal(
                userBalance.toNumber() + 10,
                newUserbalance.toNumber(),
                "User balance is not correct"
            );
        });
    });

    it("testApprove", () => {
        return ERC20.deployed().then(async token => {
            const initialAllowance = await token.allowance(
                accounts[0],
                accounts[1]
            );
            await token.approve(accounts[1], 10);
            const finalAllowance = await token.allowance(
                accounts[0],
                accounts[1]
            );
            assert.equal(
                initialAllowance.toNumber() + 10,
                finalAllowance.toNumber(),
                "Allowance does not match!"
            );
        });
    });

    it("testApprovalTransfer", () => {
        return ERC20.deployed().then(async token => {
            const ownerBalance = await token.balanceOf(accounts[0]);
            const userBalance = await token.balanceOf(accounts[1]);
            await token.transferFrom(accounts[0], accounts[1], 10);
            const newOwnerBalance = await token.balanceOf(accounts[0]);
            const newUserbalance = await token.balanceOf(accounts[1]);
            assert.equal(
                ownerBalance.toNumber() - 10,
                newOwnerBalance.toNumber(),
                "Owner balance is not correct"
            );
            assert.equal(
                userBalance.toNumber() + 10,
                newUserbalance.toNumber(),
                "User balance is not correct"
            );
        });
    })
});
