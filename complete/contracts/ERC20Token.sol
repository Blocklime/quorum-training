pragma solidity ^0.4.24;

/**
 * @title Simple ERC20 Token Implementation
 * @dev see https://eips.ethereum.org/EIPS/eip-20
 */

import "./IERC20.sol";
import "openzeppelin-solidity/contracts/math/SafeMath.sol";

contract ERC20 is IERC20 {
    using SafeMath for uint;

    uint public totalsupply;
    string public name;
    string public symbol;
    uint8 public decimals;
    address public owner;

    mapping(address=>uint) balances;
    mapping(address=> mapping (address => uint) ) allowed;

    constructor(uint _totalsupply, string _name, string _symbol, uint8 _decimals) public {

        owner = msg.sender;

        totalsupply = _totalsupply;
        balances[msg.sender] = _totalsupply;
        name = _name;
        symbol = _symbol;
        decimals = _decimals;

    }
    function name() public view returns (string){
        return name;
    }

    function symbol() public view returns (string){
        return symbol;
    }
    function decimals() public view returns (uint8){
        return decimals;
    }
    function owner() public view returns (address){
        return owner;
    }

    function totalSupply() public view returns (uint){
        return totalsupply;
    }

    function balanceOf(address _owner) public view returns (uint256 balance)
    {   
        return balances[_owner];
    }

    function allowance(address _owner, address _spender) public view returns (uint256 remaining){
        return allowed[_owner][_spender];
    }

    function transfer(address _to, uint256 _value) public returns (bool success){
        require(_to != address(0));

        balances[msg.sender] = balances[msg.sender].sub(_value);
        balances[_to] = balances[_to].add(_value);
        emit Transfer(msg.sender, _to, _value);
        return true;
    }

    function approve(address _spender, uint256 _value) public returns (bool success){
        allowed[msg.sender][_spender] = _value;
        emit Approval(msg.sender, _spender, _value);
        return true;
    }

    function transferFrom(address _from, address _to, uint256 _value) public returns (bool success){
        require(_to != address(0));

        balances[_from] = balances[_from].sub(_value);
        balances[_to] = balances[_to].add(_value);
        allowed[_from][_to] = allowed[_from][_to].sub(_value);
        emit Transfer(_from, _to, _value);
        return true;
    }


    event Transfer(address indexed _from, address indexed _to, uint256 _value);

    event Approval(address indexed _owner, address indexed _spender, uint256 _value);

}