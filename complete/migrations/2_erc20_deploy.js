var MyContract = artifacts.require("ERC20");

module.exports = function(deployer) {
    deployer.deploy(MyContract, 10000, "ERC20 Token", "ERC20", 8);
};
